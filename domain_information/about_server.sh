#! /bin/bash 

if [[ "$(whoami)" != "root" ]]; 
then
    echo "Error, please use user root"
    exit 1
fi

if [[ -z "$1"  ]]; then
	echo "Please enter domain name or ip adress"
	exit 1
fi

apt update &> /dev/null
apt install -y traceroute nmap &> /dev/null

echo "##########################################################"
echo "###### Information from the source traceroute ############"
echo "##########################################################"

traceroute $1

echo "##########################################################"
echo "######### Information from the source ping ###############"
echo "##########################################################"

ping -w 3 $1

echo "##########################################################"
echo "###### Information from the source nmap (TCP port)########"
echo "##########################################################"

nmap $1 -sT --reason

echo "##########################################################"
echo "######### Information from the source dig ################"
echo "##########################################################"

dig $1

echo "##########################################################"
echo "##########################################################"
echo "##########################################################"