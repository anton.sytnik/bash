#! /bin/bash


# The script works well on Ubuntu and Debian

#  This script will execute :
#  -- Run command "apt update && apt upgrade" ;
#  -- Install mysql nginx php-fpm ;
#  -- Download wordpress ;
#  -- Copy file wordpress ;
#  -- Create сonfiguration file ngixn ( /etc/nginx/sites-available/$DOMAIN ) ;
#  -- Create сonfiguration file wordpress ( /var/www/$DOMAIN/wp-config.php ) ;
#  -- Create mysql data base .


# Download script git clone https://gitlab.com/anton.sytnik/bash.git



echo "Please enter a NAME for the wordpress DATABASE name"
read NAME_WORDPRESS_DATABASE

echo "Please enter the USERNAME of the wordpress database"
read NAME_WORDPRESS_USER

echo "Please enter the PASSWORD of the wordpress DATABASE"
read NAME_WORDPRESS_PASSWORD

echo "Please enter a DOMAIN"
read DOMAIN


apt update && apt upgrade -y

apt install nginx -y
service nginx start
service nginx reload


apt install php-fpm php-curl php-gd php-mbstring php-xml php-xmlrpc php-mysql -y

ECHO_PHP_VERSION=$(apt list --installed | grep -i -o -m 1 php[0-9].[0-9] | grep -o [0-9].[0-9])

sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php/$ECHO_PHP_VERSION/fpm/php.ini
service php$ECHO_PHP_VERSION-fpm start
service php$ECHO_PHP_VERSION-fpm reload


# Install depends packages mysql-server 

wget https://dev.mysql.com/get/mysql-apt-config_0.8.15-1_all.deb
apt update
apt install -y debconf dpkg lsb-release wget bash gnupg
dpkg -i mysql-apt-config_0.8.15-1_all.deb
rm -f mysql-apt-config_0.8.15-1_all.deb

apt update
apt install -y mysql-server
service mysql start
service mysql reload

mysql_secure_installation

echo "
[mysqld]
skip-networking
" >> /etc/mysql/mysql.conf.d/mysqld.cnf


mkdir /var/www/$DOMAIN
cp -v /etc/nginx/sites-available/default /etc/nginx/sites-available/$DOMAIN
ln -s /etc/nginx/sites-available/$DOMAIN /etc/nginx/sites-enabled/$DOMAIN

echo "
server {
	listen 80;

	root /var/www/$DOMAIN;

	index index.php index.html index.htm;

	server_name $DOMAIN;
	charset utf-8;
	location / {
	try_files \$uri \$uri/ /index.php?\$args;
	}

	location = /favicon.ico { 
		log_not_found off; access_log off; 
	}

	location = /robots.txt { 
		log_not_found off; access_log off; allow all; 
	}

	location ~* \.(css|gif|ico|jpeg|jpg|js|png)$ {
		expires max;
		log_not_found off;
	}

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
	}

	location ~ /\.ht {
		deny all;
	}
}" > /etc/nginx/sites-available/$DOMAIN


wget http://wordpress.org/latest.tar.gz -P /root
tar -xzf /root/latest.tar.gz -C /root
cp -r /root/wordpress/* /var/www/$DOMAIN
find /var/www/$DOMAIN -type d -exec chmod 755 {} \;
find /var/www/$DOMAIN -type f -exec chmod 644 {} \;
chown -R www-data:www-data /var/www/$DOMAINS


echo "
CREATE DATABASE $NAME_WORDPRESS_DATABASE;
CREATE USER $NAME_WORDPRESS_USER@localhost IDENTIFIED BY '$NAME_WORDPRESS_PASSWORD';
GRANT ALL PRIVILEGES ON $NAME_WORDPRESS_DATABASE.* TO $NAME_WORDPRESS_USER@localhost;
FLUSH PRIVILEGES;
SHOW DATABASES;
" > /root/$WPDATABASE

mysql -u root -p < /root/$NAME_WORDPRESS_DATABASE
rm /root/$NAME_WORDPRESS_DATABASE
rm -r /root/wordpress/
rm /root/latest.tar.gz

cp /var/www/$DOMAIN/wp-config-sample.php /var/www/$DOMAIN/wp-config.php

curl -s https://api.wordpress.org/secret-key/1.1/salt/ | cat > /root/wp_temp_file

sed -i s/database_name_here/$NAME_WORDPRESS_DATABASE/g /var/www/$DOMAIN/wp-config.php 
sed -i s/username_here/$NAME_WORDPRESS_USER/g /var/www/$DOMAIN/wp-config.php 
sed -i s/password_here/$NAME_WORDPRESS_PASSWORD/g /var/www/$DOMAIN/wp-config.php 
sed -i '49,56d' /var/www/$DOMAIN/wp-config.php
sed -i '49r /root/wp_temp_file' /var/www/$DOMAIN/wp-config.php

rm /root/wp_temp_file

echo "name wpdatabase = $NAME_WORDPRESS_DATABASE"
echo "wpuser = $NAME_WORDPRESS_USER"
echo "wppassword = $NAME_WORDPRESS_PASSWORD"
echo "Domain = $DOMAIN"