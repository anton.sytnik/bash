#! /bin/bash


# The script works well on Ubuntu and Debian

#  This script will execute :
#  -- Create a backup folder;
#  -- Copy log files;
#  -- Copy site files;
#  -- Will create a log file of the script.

# Download script git clone https://gitlab.com/anton.sytnik/bash.git


DIR_BACKUP='/backup'
DIR_BACKUP_LOG='/var/log/backup-log'
DIR_TMP='/var/tmp/backup'
DIR_SITE='/var/www/antondev.com.ua'
FILE_BACKUP_LOG='backup-script.log'
TIME_FORMAT='date +[%A-%b-%d-%m-%Y|%T]'


##########################################################################################
##########################################################################################
########################               Function              #############################
##########################################################################################
##########################################################################################

function error {
	echo " $($TIME_FORMAT)===>  Удаление $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	rm $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)
}

##########################################################################################
##########################################################################################
########################          End     function              ##########################
##########################################################################################
##########################################################################################


if [[ ! -e $DIR_BACKUP_LOG ]]; then
	mkdir $DIR_BACKUP_LOG 
	touch $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  First use of the script! " >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi

if [[ ! -e $DIR_BACKUP ]]; then 
	mkdir -v $DIR_BACKUP &>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Create dir backup $DIR_BACKUP" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Create dir log $DIR_BACKUP_LOG" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Create log file backup $DIR_BACKUP_LOG/$FILE_BACKUP_LOG" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi

if [[ ! -e $DIR_TMP ]]; then 
	mkdir -v $DIR_TMP &>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Creating a temporary folder for mysqldump! $DIR_TMP" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

fi


echo " $($TIME_FORMAT)===>  Script backup launch" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG


if [ $(df -h | grep -w '/' | tr -s ' ' | cut -d ' ' -f 5 | sed 's/%//g') -gt 95 ]; then
	echo " $($TIME_FORMAT)===>  Error " >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  You have too little hard disk space" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	exit 1
else
	echo "Свободное место на диске ==>  $(df -h | grep -w '/' | tr -s ' ' | cut -d ' ' -f 5)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi


echo " $($TIME_FORMAT)===>  Create archive backup $DIR_BACKUP/archive_backup_$(date +%d-%m-%Y-%T).tar" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG


tar -C /etc -cvf "$DIR_BACKUP/backup_archive_$(date +"%d-%m-%Y-%H-%M-%S").tar" nginx mysql php &>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

if [ "$?" != 0 ]; then
	echo " $($TIME_FORMAT)===>  Error creating backup $(date +"%d-%m-%Y-%H-%M-%S").tar" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Stop use script!" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	error
	exit 1
else
	echo " $($TIME_FORMAT)===>  Files added to archive $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi


echo " $($TIME_FORMAT)===>  Files log added to archive $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

tar --append --file=$DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1) /var/log/nginx /var/log/mysql /var/log/php7.2-fpm.log &>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

if [ "$?" != 0 ]; then
	echo " $($TIME_FORMAT)===>  Error adding files log to archive $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Stop use script!" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	error
	exit 1
else
	echo " $($TIME_FORMAT)===>  Finish files log added to archive $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi


echo " $($TIME_FORMAT)===>  Creating database backup $DIR_TMP/backup_msql_databases_$(date +%d-%m-%Y-%T).sql" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

mysqldump -v -u root --all-databases > $DIR_TMP/backup_msql_databases_$(date +%d-%m-%Y-%T).sql 2>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

if [ "$?" != 0 ]; then
	echo " $($TIME_FORMAT)===>  Error creating database backup $DIR_TMP/$(ls -t $DIR_TMP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Stop use script!" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	error
	exit 1
else
	echo " $($TIME_FORMAT)===>  Creating database finish $DIR_TMP/$(ls -t $DIR_TMP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi


echo " $($TIME_FORMAT)===>  Add database backup in $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

tar -C $DIR_TMP --append --file=$DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)  $(ls -t $DIR_TMP | head -1) &>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

if [ "$?" != 0 ]; then
	echo " $($TIME_FORMAT)===>  Error adding files to archive $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Stop use script!" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	error
	exit 1
else
	echo " $($TIME_FORMAT)===>  Files added to archive $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi


echo " $($TIME_FORMAT)===>  Delete temporary files $DIR_TMP/$(ls -t $DIR_TMP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

rm -v $DIR_TMP/$(ls -t $DIR_TMP | head -1) &>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

if [ "$?" != 0 ]; then
	echo " $($TIME_FORMAT)===>  Error deleting temporary files $DIR_TMP/$(ls -t $DIR_TMP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Stop use script!" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	error
	exit 1
else
	echo " $($TIME_FORMAT)===>  Uninstall Complete $DIR_TMP/$(ls -t $DIR_TMP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi


echo " $($TIME_FORMAT)===>  Add folder site $DIR_SITE to archive $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG


tar --append --file=$DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1) $DIR_SITE &>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

if [ "$?" != 0 ]; then
	echo " $($TIME_FORMAT)===>  Error adding site to archive $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Stop use script!" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	error
	exit 1
else
	echo " $($TIME_FORMAT)===>  Finish added site to archive $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi


TMP_TAR="$DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)"


echo " $($TIME_FORMAT)===>  Archive compression $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

tar -C $DIR_BACKUP -zcvf $DIR_BACKUP/backup_archive_$(date +"%d-%m-%Y-%H-%M-%S").tar.gz $(ls -t $DIR_BACKUP | head -1) &>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

if [ "$?" != 0 ]; then
	echo " $($TIME_FORMAT)===>  Error archive compression $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Stop use script!" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	error
	exit 1
else
	echo " $($TIME_FORMAT)===>  Finish archive compression $DIR_BACKUP/$(ls -t $DIR_BACKUP | head -1)" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi


rm -v $TMP_TAR &>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

if [ "$?" != 0 ]; then
	echo " $($TIME_FORMAT)===>  Error deleting temporary tar $TMP_TAR" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Stop use script!" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	error
	exit 1
else
	echo " $($TIME_FORMAT)===>  Uninstall Complete $TMP_TAR" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi


OLD_BACKUP="$(ls -t $DIR_BACKUP | tail -n 5 | tail -n 1)"


if [ "$(ls -t $DIR_BACKUP | tail -n 5 | wc -l)" -ge 5 ]; then
	rm -v $DIR_BACKUP/$OLD_BACKUP &>> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	echo " $($TIME_FORMAT)===>  Delete old backup $DIR_BACKUP/$OLD_BACKUP" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG

	if [ "$?" != 0 ]; then
		echo " $($TIME_FORMAT)===>  Error delete old backup $DIR_BACKUP/$OLD_BACKUP" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
		echo " $($TIME_FORMAT)===>  Stop use script!" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
		error
		exit 1
	else
		echo " $($TIME_FORMAT)===>  Delete complete $DIR_BACKUP/$OLD_BACKUP" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
		echo " $($TIME_FORMAT)===>  Finish script backup" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
	fi
	else
		echo " $($TIME_FORMAT)===>  Finish script backup" >> $DIR_BACKUP_LOG/$FILE_BACKUP_LOG
fi