The script works well on Ubuntu and Debian

 **This script will execute :**

* -- Run command "apt update && apt upgrade" ;
* -- Install vim nano tree bash-completion curl wget git ;
* -- Add a alias for selected users ;
* -- Add "bash-completion" in /root/.bashrc ;
* -- Change user colors; 
* -- Will do SSH authorization by key ;
* -- Copy files from user home directories ;
* -- Copy files from /etc.

*Download script git clone https://gitlab.com/anton.sytnik/bash.git*