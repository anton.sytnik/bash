#! /bin/bash


# This script will execute :
# -- Run command "apt update && apt upgrade" ;
# -- Install vim nano tree bash-completion curl wget git ;
# -- Add a alias for selected users ;
# -- Add "bash-completion" in /root/.bashrc ;
# -- Change user colors; 
# -- Will do SSH authorization by key ;
# -- Copy files from user home directories ;
# -- Copy files from /etc. 


# Download script git clone https://gitlab.com/anton.sytnik/bash.git


##########################################################################################
##########################################################################################
########################               Function              #############################
##########################################################################################
##########################################################################################

function USER_UPDATE {
    mkdir /backup_files_$USER_NAME
    cp -rav /home/$USER_NAME/. /backup_files_$USER_NAME

    echo "" >> /home/$USER_NAME/.bashrc
    echo '#USER SETTINGS' >> /home/$USER_NAME/.bashrc
    echo '' >> /home/$USER_NAME/.bashrc
    echo 'HISTTIMEFORMAT="(%a)%d/%m/%y | %T "' >> /home/$USER_NAME/.bashrc
    echo "alias c='clear'" >> /home/$USER_NAME/.bashrc
    echo "alias grep='grep --color'" >> /home/$USER_NAME/.bashrc
    echo "alias ll='ls -alhF --color'" >> /home/$USER_NAME/.bashrc
    echo "alias v='vim'" >> /home/$USER_NAME/.bashrc
    echo "alias n='nano'" >> /home/$USER_NAME/.bashrc
    echo "#PS1 function" >> /home/$USER_NAME/.bashrc
    echo "PS1=\"\[\$(tput bold)\]\[\$(tput setaf 6)\]\t \[\$(tput setaf 2)\][\[\$(tput setaf 3)\]\u\[\$(tput setaf 1)\]@\\[\$(tput setaf 3)\]\h \[\$(tput setaf 6)\]\W\[\$(tput setaf 2)\]]\[\$(tput setaf 4)\]\\\\$ \[\$(tput sgr0)\]\" " >> /home/$USER_NAME/.bashrc 
    echo '
man() {
    env \
        LESS_TERMCAP_md=$(printf "\e[1;31m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;32m") \
        man "$@"
}
    ' >> /home/$USER_NAME/.bashrc
}

function USER_AND_FILE_VERIFICATION {
    until [[ -n $USER_NAME ]]; 
        do
            echo "Error!"
            read -p "Enter username  ==> (CTRL + C ==> exit) "  USER_NAME
    done

    while : 
        do
        if [[ $USER_NAME = '' ]] ; then
            echo "Error, user "$USER_NAME" does not exist ...!"
            read -p "Enter username  ==> (CTRL + C ==> exit) "  USER_NAME
        elif [[ "$(cat /etc/passwd | cut -d ":" -f 1 | grep -w "$USER_NAME")" = "$USER_NAME" ]]; then
            break
        else
            echo "Error, user "$USER_NAME" does not exist ...!"
            read -p "Enter username  ==> (CTRL + C ==> exit) "  USER_NAME
        fi
    done

    if [[ ! -d $(cat /etc/passwd | grep -w "$USER_NAME" | cut -d ":" -f 6) ]]; then
        echo "Directory does not exist !"
        echo "Exit"
        break
    fi
}

##########################################################################################
##########################################################################################
########################          End     function              ##########################
##########################################################################################
##########################################################################################


if [[ "$(whoami)" != "root" ]]; 
then
    echo "Error, please use user root"
    exit 1
fi


apt update && apt upgrade || exit 
apt install vim nano tree bash-completion curl wget git


read -p "Change ssh authorization to authorization key  ? ( y,Y | n,N | CTRL + C ==> exit ) ==> "  SSH_QUESTION

while :
do
    until [[ "$SSH_QUESTION" =~ ^[yYnN] ]] || [[ $SSH_QUESTION = '' ]]
            do
                echo "Не правильный ввод, пожалуйста повторите еще раз!"
                read -p "Change ssh authorization to authorization key  ? ( y,Y | n,N | CTRL + C ==> exit ) ==> " SSH_QUESTION
            done
    case "$SSH_QUESTION" in
        y | Y | '' )
                if [[ ! -f /etc/ssh/sshd_config ]]; then
                    echo "Error, file does not exist ..."
                    break
                fi 
                sed -e 's/PasswordAuthentication yes/PasswordAuthentication no/g' -i /etc/ssh/sshd_config 
                break
                ;;
        n | N ) break
                ;;
    esac
done


read -p "Update root user data ? ( y,Y | n,N ) ==> "  ROOT_QUESTION

while :
do
    until [[ "$ROOT_QUESTION" =~ ^[yYnN] ]] || [[ $ROOT_QUESTION = '' ]]
            do
                echo "Invalid input, please try again!"
                read -p "Update root user data ? ( y,Y | n,N | CTRL + C ==> exit ) ==> " ROOT_QUESTION
            done
    case "$ROOT_QUESTION" in
        y | Y | '' ) 
                mkdir /backup_files_root
                mkdir /backup_files_root/etc

                cp -rav /root/. /backup_files_root
                cp -rav /etc/. /backup_files_root/etc

                echo "#USER SETTTINGS"
                echo ":set number" >> /etc/vim/vimrc
                echo "" >> /root/.bashrc
                echo "#USER SETTINGS" >> /root/.bashrc
                echo 'HISTTIMEFORMAT="(%a)%d/%m/%y | %T "' >> /root/.bashrc
                echo "alias c='clear'" >> /root/.bashrc
                echo "alias grep='grep --color'" >> /root/.bashrc
                echo "alias ll='ls -alhF --color'" >> /root/.bashrc
                echo "alias n='nano'" >> /root/.bashrc
                echo "alias v='vim'" >> /root/.bashrc
                echo '#PS1 function' >> /root/.bashrc
                echo 'PS1="\[$(tput bold)\]\[\033[38;5;160m\]\t\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;160m\]\u\[$(tput sgr0)\]\[\033[38;5;44m\]@\[$(tput sgr0)\]\[\033[38;5;2m\]\h\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;160m\]\w\[$(tput sgr0)\] \\$ \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;160m\]----->   \[$(tput sgr0)\]"' >> /root/.bashrc
                echo '. /usr/share/bash-completion/bash_completion' >> /root/.bashrc
                echo "#color man" >> /root/.bashrc
                echo '
                man() {
                  env \
                    LESS_TERMCAP_md=$(printf "\e[1;31m") \
                    LESS_TERMCAP_me=$(printf "\e[0m") \
                    LESS_TERMCAP_se=$(printf "\e[0m") \
                    LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
                    LESS_TERMCAP_ue=$(printf "\e[0m") \
                    LESS_TERMCAP_us=$(printf "\e[1;32m") \
                    man "$@"
                }
                ' >> /root/.bashrc
                break
                ;;
        n | N ) break
                ;;
    esac
done


read -p "Would you like to update another user's data? ( y,Y | n,N | CTRL + C ==> exit ) ==> "  USER_QUESTION

while :
    do
        until [[ "$USER_QUESTION" =~ ^[yYnN] ]] || [[ $USER_QUESTION = '' ]]
            do
                echo "Invalid input, please try again!"
                read -p "Would you like to update another user's data? ( y,Y | n,N | CTRL + C ==> exit ) ==> " USER_QUESTION
        done
        case "$USER_QUESTION" in
            y | Y | '' ) read -p "Enter the username for which you want to update the information ==> "  USER_NAME

                    USER_AND_FILE_VERIFICATION

                    USER_UPDATE
                    break
                    ;;
            n | N ) echo "Ok, bye "
                    exit 0
                    ;;
    esac
done


read -p "Still update the data of any user ? ( y,Y | n,N | CTRL + C ==> exit ) ==> "  USER_QUESTION

while :
    do
        until [[ "$USER_QUESTION" =~ ^[yYnN] ]] || [[ $USER_QUESTION = '' ]]
            do
                echo "Invalid input, please try again!"
                read -p "Still update the data of any user ? ( y,Y | n,N | CTRL + C ==> exit ) ==> " USER_QUESTION
            done
        case "$USER_QUESTION" in
        y | Y | '' ) read -p "Enter the username for which you want to update the information (CTRL + C ==> exit) ==> "  USER_NAME

                USER_AND_FILE_VERIFICATION

                USER_UPDATE
                continue
                ;;
        n | N ) echo "Ok, bye "
                exit 0
                ;;
    esac
done
